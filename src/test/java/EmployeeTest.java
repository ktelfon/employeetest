import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class EmployeeTest {

    Employee employee;

    @BeforeEach
    void setUp() {
        employee = new Employee();
    }

    @Test
    void setSalaryCorrectAmount() throws Exception {
        double expectedSalary = 101.0;
        employee.setSalary(expectedSalary);
        assertEquals(expectedSalary, employee.getSalary());
    }

    @Test
    void setSalaryNegativeAmount() {
       Exception exception = assertThrows(Exception.class, () -> employee.setSalary(-1.0));
       assertEquals(Messages.NEGATIVE_SALARY_EXCEPTION, exception.getMessage());
    }

    @Test
    void setSalaryMinimalAmount() {
        Exception exception = assertThrows(Exception.class, () -> employee.setSalary(89.0));
        assertEquals(Messages.MINIMAL_SALARY_EXCEPTION, exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("getResults")
    void setSalary(double salary, String errorMsg){
        Exception exception = assertThrows(Exception.class, () -> employee.setSalary(salary));
        assertEquals(errorMsg, exception.getMessage());
    }

    private static Stream<Arguments> getResults() {
        return Stream.of(
                Arguments.of(-1.0, Messages.NEGATIVE_SALARY_EXCEPTION),
                Arguments.of(89.0, Messages.MINIMAL_SALARY_EXCEPTION)
        );
    }

    @Test
    void setAgeCorrectAmount() throws Exception {
        int expectedAge = 1;
        employee.setAge(expectedAge);
        assertEquals(expectedAge, employee.getAge());
    }

    @Test
    void setAgeNegativeAmount()  {
        Exception exception = assertThrows(Exception.class, () -> employee.setAge(-1));
        assertEquals(Messages.NEGATIVE_AGE_EXCEPTION, exception.getMessage());
    }

    @Test
    void setEmailCorrectFormat() throws Exception {
        String expectedEmail = "blalal@blala.lv";
        employee.setEmail(expectedEmail);
        assertEquals(expectedEmail, employee.getEmail());
    }

    @Test
    void setEmailWrongFormat() {
        Exception exception = assertThrows(Exception.class, () -> employee.setEmail("blablalbal.lblb"));
        assertEquals(Messages.WRONG_EMAIL, exception.getMessage());
    }

    @Test
    void setPhoneNumberCorrectFormat() throws Exception {
        String expectedPhone = "9098238763";
        employee.setPhoneNumber(expectedPhone);
        assertEquals(expectedPhone, employee.getPhoneNumber());
    }

    @Test
    void setPhoneNumberWrongFormat() {
        Exception exception = assertThrows(
                Exception.class,
                () -> employee.setPhoneNumber("i273yregfhjsb4738ryegjwhf6truiyewgf"));
        assertEquals(Messages.WRONG_PHONE_NUMBER, exception.getMessage());
    }
}