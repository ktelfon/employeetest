import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Employee {

//    Create simple Employee class. Write common setters and getter for fields like salary,
//    age and so on.
//            • Using parametrized tests write boundary tests (like negative salary, minimal
//            positive salary, negative age).
//            • Check if you can set e-mail address in wrong format.
//   Check if you can set phone number in wrong format.

    public static final double minimalSalary = 100;

    private double salary;
    private int age;
    private String email;
    private String phoneNumber;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) throws Exception {

        if (salary < 0) {
            throw new Exception(Messages.NEGATIVE_SALARY_EXCEPTION);
        }

        if (salary < minimalSalary) {
            throw new Exception(Messages.MINIMAL_SALARY_EXCEPTION);
        }

        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {

        if (age < 0) {
            throw new Exception(Messages.NEGATIVE_AGE_EXCEPTION);
        }

        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws Exception {

        String regex = "^(.+)@(.+)$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(email);

        if(!matcher.matches()){
           throw new Exception(Messages.WRONG_EMAIL);
        }

        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) throws Exception {

        Pattern pattern = Pattern.compile("^\\d{10}$");
        Matcher matcher = pattern.matcher(phoneNumber);

        if(!matcher.matches()){
            throw new Exception(Messages.WRONG_PHONE_NUMBER);
        }

        this.phoneNumber = phoneNumber;
    }
}
