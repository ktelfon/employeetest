public class Messages {

    public final static String NEGATIVE_SALARY_EXCEPTION = "Salary can't be negative";
    public static final String MINIMAL_SALARY_EXCEPTION = "Salary is too low";
    public static final String NEGATIVE_AGE_EXCEPTION = "Age can't be negative";
    public static final String WRONG_EMAIL = "Wrong email format";
    public static final String WRONG_PHONE_NUMBER = "Wrong phone format";
}
